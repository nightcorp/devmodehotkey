﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace DevModeHotkey
{
    [DefOf]
    public static class NCDMHK_KeyBindingDefOf
    {
        static NCDMHK_KeyBindingDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(NCDMHK_KeyBindingDefOf));
        }

        public static KeyBindingDef NCDMHK_DevelopmentModeToggle;
    }
}
