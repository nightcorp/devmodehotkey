﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace DevModeHotkey
{
    [HarmonyPatch(typeof(Root), nameof(Root.OnGUI))]
    public static class Patch_UIRoot_Play
    {
        [HarmonyPostfix]
        public static void AddHookForDevHotkey()
        {
            try
            {
                if (NCDMHK_KeyBindingDefOf.NCDMHK_DevelopmentModeToggle.KeyDownEvent)
                {
                    Prefs.DevMode = !Prefs.DevMode;
                }
            }
            catch(Exception ex)
            {
                string message = $"Caught exception when trying to listen for dev-mode hotkey press: {ex}";
                Log.ErrorOnce(message, message.GetHashCode());
            }
        }
    }
}
